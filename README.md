# Tuwel Quiz Gen
This is a Question-Generator for EP1 at TU Wien
# Usage
`java -jar TuwelQuizGen-0.1.jar <path_to_questions_folder> <out_name>`
## Files
For one question there are 3 files needed.

A `false.txt` where the false answers are stored

A `true.txt` where the true answers are stored

A `title.txt` where the title and meta data for the question is stored (atm this does not work)
## Syntax
### false.txt and true.txt
* A question ends after two line breaks
* For displaying multi line code use \`\`\`CODE\`\`\`
* In a multi line code block multiple line breaks can be used (atm this does not work)
* For displaying inline code use \`CODE\`

[example](syntax_example.txt)


### title.txt
TODO
# Download
[Version 0.1](https://drive.google.com/file/d/0By8Tmqsx4RyHZUhBSzRmbElHNjg/view?usp=sharing)
