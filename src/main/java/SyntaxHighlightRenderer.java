import org.commonmark.node.Code;
import org.commonmark.node.FencedCodeBlock;
import org.commonmark.node.Node;
import org.commonmark.renderer.NodeRenderer;
import org.commonmark.renderer.html.HtmlNodeRendererContext;
import org.commonmark.renderer.html.HtmlWriter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by raphael on 01.05.17.
 */
public class SyntaxHighlightRenderer implements NodeRenderer {


    private final HtmlWriter html;

    SyntaxHighlightRenderer(HtmlNodeRendererContext context) {
        this.html = context.getWriter();
    }

    @Override
    public Set<Class<? extends Node>> getNodeTypes() {
        Set<Class<? extends Node>> nodes = new HashSet<>();
        nodes.add(FencedCodeBlock.class);
        nodes.add(Code.class);

        return nodes;
    }

    @Override
    public void render(Node node) {

        String literal = null;
        boolean isOneLine = false;
        if(node instanceof FencedCodeBlock) {
            FencedCodeBlock codeBlock = (FencedCodeBlock) node;
            literal = codeBlock.getLiteral();
        } else if ( node instanceof Code) {
            Code code = (Code) node;
            literal = code.getLiteral();
            isOneLine = true;
        } else {
            return;
        }


        if(!isOneLine) {
            html.tag("pre");
        }
        html.line();
        html.tag("code class=\"language-java\"");
        String[] lines = literal.split("\n");

        for (String s : lines) {
            String[] tokens = s.split(" ");
            boolean comment = false;
            for (String token : tokens) {
                // TODO add braces like do{ } vs do { }
                if (token.matches("^int|double|float|String|Boolean|byte|short|long|public|void|static|return|for|while|if|else|do|null|import")) {
                    printHighlightBlue(token);
                } else if (token.matches("\".*")) {
                    printStringStart(token);
                } else if (token.matches(".*\";?")) {
                    printStringEnd(token);
                } else if (token.matches("//")) {
                    printCommentStart(token);
                    comment = true;
                } else {
                    html.text(token);
                }
                html.text(" ");
            }
            printCommentEnd(comment);
            html.line();
        }

        html.tag("/code");
        html.line();
        if(!isOneLine) {
            html.tag("/pre");
        }
    }

    private void printCommentStart(String literal) {
        html.tag("span style=\"color: rgb(128,128,128); font-style: italic;\"");
        html.text(literal);
    }

    private void printCommentEnd(boolean comment) {
        if (comment) {
            html.tag("/span");
        }
    }

    private void printStringStart(String literal) {
        html.tag("span style=\"color: rgb(0,128,0); font-weight: bold;\"");
        html.text(literal);
    }

    private void printStringEnd(String literal) {
        if (literal.endsWith(";")) {
            html.text(literal.substring(0, literal.length() - 1));
            html.tag("/span");
            html.text(";");
        } else {
            html.text(literal);
            html.tag("/span");
        }
    }

    private void printHighlightBlue(String literal) {
        html.tag("span style=\"color: rgb(0,0,128); font-weight: bold;\"");
        html.text(literal);
        html.tag("/span");
    }
}
