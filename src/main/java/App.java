import java.io.File;
import java.util.List;

/**
 * Created by raphael on 10.04.17.
 */
public class App {


    public static void main(String[] args) {

        if (args.length != 2) {
            usage();
            return;
        }

        File file = new File(args[0]);
        if (!file.isDirectory()) {
            usage();
            return;
        }

        File[] trueFiles = file.listFiles(pathname -> {
            if (pathname.isDirectory()) {
                return false;
            }
            if (!pathname.getName().equals("true.txt")) {
                return false;
            }
            return true;
        });

        File[] falseFiles = file.listFiles(pathname -> {
            if (pathname.isDirectory()) {
                return false;
            }
            if (!pathname.getName().equals("false.txt")) {
                return false;
            }
            return true;
        });

        File[] titleFiles = file.listFiles(pathname -> {
            if (pathname.isDirectory()) {
                return false;
            }
            if (!pathname.getName().equals("title.txt")) {
                return false;
            }
            return true;
        });

        if (trueFiles.length < 1 || falseFiles.length < 1 || titleFiles.length < 1) {
            usage();
            return;
        }

        String path = args[1];
        if (!path.endsWith("xml")) {
            path += ".xml";
        }


        List<String> answersCorrect = Utils.read(trueFiles[0].getAbsolutePath());
        List<String> answersWrong = Utils.read(falseFiles[0].getAbsolutePath());
        // TODO Title

        answersWrong = Utils.convert(answersWrong);
        answersCorrect = Utils.convert(answersCorrect);
        
        ModelXmlPrinter.print(answersCorrect, answersWrong, path);
    }

    private static void usage() {
        System.out.println("java -jar TuwelQuizGen-0.1.jar <path_to_questions_folder> <out_name>");
    }

}
