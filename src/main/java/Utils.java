import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.NodeRenderer;
import org.commonmark.renderer.html.HtmlNodeRendererContext;
import org.commonmark.renderer.html.HtmlNodeRendererFactory;
import org.commonmark.renderer.html.HtmlRenderer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by raphael on 24.04.17.
 */
public class Utils {

    /**
     * Reads all answers from the File with the path path.
     * answers are separated with an empty line(only a \n in it)
     *
     * @param path of the File
     * @return list of all answers
     */
    public static List<String> read(String path) {
        List<String> data = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line = br.readLine();
            String question = "";

            while (line != null) {
                if (line.isEmpty()) {
                    data.add(question);
                    question = "";
                } else {
                    question += "\n" + line;
                }
                line = br.readLine();
            }
            // add last line
            data.add(question);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    /**
     * Converts a list of Markdown Strings to html
     *
     * @param data
     * @return
     */
    public static List<String> convert(List<String> data) {
        List<String> result = new ArrayList<>();

        data.forEach(s -> {
            Parser parser = Parser.builder().build();
            Node document = parser.parse(s);

            HtmlRenderer renderer = HtmlRenderer.builder()
                    .nodeRendererFactory(new HtmlNodeRendererFactory() {
                        public NodeRenderer create(HtmlNodeRendererContext context) {
                            return new SyntaxHighlightRenderer(context);
                        }
                    })
                    .build();
            String r = renderer.render(document);
            result.add(r);
        });


        return result;
    }
}
