package model;

/**
 * Created by raphael on 10.04.17.
 */
public class Answer {

    private String text;
    private String fraction;
    private String feedback;

    public Answer(String text, String fraction, String feedback) {
        this.text = text;
        this.fraction = fraction;
        this.feedback = feedback;
    }

    public Answer(String text, String fraction) {
        this.text = text;
        this.fraction = fraction;
        this.feedback = "";
    }

    public String getFeedback() {
        return feedback;
    }

    public String getText() {
        return text;
    }

    public String getFraction() {
        return fraction;
    }
}
