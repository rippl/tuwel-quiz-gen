import model.Answer;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by raphael on 09.05.17.
 */
public class ModelXmlPrinter {

    private static final int NUMBER_OF_ANSWERS = 6;

    private static final String QUIZ_TAG = "quiz";
    private static final String QUESTION_TAG = "question";
    private static final String ANSWER_TAG = "answer";
    private static final String QUESTION_TYPE = "multichoice";

    private static String QUESTION_NAME = "Operatoren - Kommentare";
    private static String QUESTION_TEXT = "<p>Welche der Aussagen sind richtig?</p>";
    private static String QUESTION_GENERAL_FEEDBACK = "<p>This is a Feedback</p>";
    private static String QUESTION_CORRECT_FEEDBACK = "Die Antwort ist richtig";
    private static String QUESTION_PARTIALLY_CORRECT_FEEDBACK = "Die Antwort ist teilweise richtig.";
    private static String QUESTION_INCORRECT_FEEDBACK = "Die Antwort ist falsch";

    private static final String FEEDBACK = "feedback";
    private static final String TEXT = "text";
    private static final String FORMAT = "format";
    private static final String FORMAT_VALUE = "html";
    private static final String FRACTION = "fraction";

    public static void print(List<String> answersCorrect, List<String> answersWrong, String path) {

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();
            Element quiz = document.createElement(QUIZ_TAG);
            document.appendChild(quiz);

            for (int i = 0; i < 10; i++) {
                // gen answer set
                ArrayList<String> t = new ArrayList<>(answersCorrect);
                ArrayList<String> f = new ArrayList<>(answersWrong);

                ArrayList<Answer> answers = selectAnswers(t, f, NUMBER_OF_ANSWERS, 3); // TODO 3-3, 4-2(25%, -50%), 2-4 (50%, -25%)
                quiz.appendChild(genQuestion(document, answers));
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(path));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);

            System.out.println("File saved!");


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }


    private static Element genQuestion(Document document, ArrayList<Answer> answers) {

        Element question = document.createElement(QUESTION_TAG);

        // add question type
        Attr attr = document.createAttribute("type");
        attr.setValue(QUESTION_TYPE);
        question.setAttributeNode(attr);

        Element name = document.createElement("name");
        question.appendChild(name);

        Element text = document.createElement(TEXT);
        text.appendChild(document.createTextNode(QUESTION_NAME));
        name.appendChild(text);

        // Question Text
        Element questionText = document.createElement("questiontext");
        question.appendChild(questionText);

        attr = document.createAttribute(FORMAT);
        attr.setValue(FORMAT_VALUE);
        questionText.setAttributeNode(attr);

        text = document.createElement(TEXT);
        questionText.appendChild(text);
        text.appendChild(document.createCDATASection(QUESTION_TEXT));

        // Generalfeedback
        Element generalFeedback = document.createElement("generalfeedback");
        question.appendChild(generalFeedback);

        attr = document.createAttribute(FORMAT);
        attr.setValue(FORMAT_VALUE);
        generalFeedback.setAttributeNode(attr);

        text = document.createElement(TEXT);
        generalFeedback.appendChild(text);
        text.appendChild(document.createCDATASection(QUESTION_GENERAL_FEEDBACK));

        Element element = document.createElement("defaultgrade");
        question.appendChild(element);
        element.appendChild(document.createTextNode("1.0000000"));

        element = document.createElement("penalty");
        question.appendChild(element);
        element.appendChild(document.createTextNode("0.3333333"));

        element = document.createElement("hidden");
        question.appendChild(element);
        element.appendChild(document.createTextNode("0"));

        element = document.createElement("single");
        question.appendChild(element);
        element.appendChild(document.createTextNode("false"));

        element = document.createElement("shuffleanswers");
        question.appendChild(element);
        element.appendChild(document.createTextNode("true"));

        element = document.createElement("answernumbering");
        question.appendChild(element);
        element.appendChild(document.createTextNode("none"));

        // Generalfeedback
        element = document.createElement("generalfeedback");
        question.appendChild(element);
        attr = document.createAttribute(FORMAT);
        attr.setValue(FORMAT_VALUE);
        element.setAttributeNode(attr);
        text = document.createElement(TEXT);
        element.appendChild(text);
        text.appendChild(document.createTextNode(QUESTION_CORRECT_FEEDBACK));

        // correctfeedback
        element = document.createElement("correctfeedback");
        question.appendChild(element);
        attr = document.createAttribute(FORMAT);
        attr.setValue(FORMAT_VALUE);
        element.setAttributeNode(attr);
        text = document.createElement(TEXT);
        element.appendChild(text);
        text.appendChild(document.createTextNode(QUESTION_PARTIALLY_CORRECT_FEEDBACK));

        // incorrectfeedback
        element = document.createElement("incorrectfeedback");
        question.appendChild(element);
        attr = document.createAttribute(FORMAT);
        attr.setValue(FORMAT_VALUE);
        element.setAttributeNode(attr);
        text = document.createElement(TEXT);
        element.appendChild(text);
        text.appendChild(document.createTextNode(QUESTION_INCORRECT_FEEDBACK));

        element = document.createElement("shownumcorrect");
        question.appendChild(element);

        // add answers
        for (Answer a : answers) {
            question.appendChild(genAnswer(document, a));

        }

        return question;
    }

    private static Element genAnswer(Document document, Answer answer) {
        String fraction = answer.getFraction();
        String text = answer.getText();
        String feedback = answer.getFeedback();

        Element answerElem = document.createElement(ANSWER_TAG);

        // add fraction value
        Attr attr = document.createAttribute(FRACTION);
        attr.setValue(fraction);
        answerElem.setAttributeNode(attr);

        // add html attr
        attr = document.createAttribute(FORMAT);
        attr.setValue(FORMAT_VALUE);
        answerElem.setAttributeNode(attr);

        // add answer text
        Element answerText = document.createElement(TEXT);
        answerElem.appendChild(answerText);
        answerText.appendChild(document.createCDATASection(text));

        // add feedback elem
        Element feedbackElement = document.createElement(FEEDBACK);
        answerElem.appendChild(feedbackElement);

        // set format for feedback
        attr = document.createAttribute(FORMAT);
        attr.setValue(FORMAT_VALUE);
        feedbackElement.setAttributeNode(attr);

        // set feedback text
        Element feedbackText = document.createElement(TEXT);
        feedbackElement.appendChild(feedbackText);
        feedbackText.appendChild(document.createTextNode(feedback));

        return answerElem;
    }

    // simple rnd select
    static private ArrayList<Answer> selectAnswers(ArrayList<String> correct, ArrayList<String> incorrect, int numberOfAnswers, int numberOfCorrect) {

        ArrayList<Answer> answers = new ArrayList<>();

        // select fraction
        // TODO calculate fraction
        String fraction = "33.33333";
        String fractionNeg = "-" + fraction;

//        // TODO prevent 0 correct
//        int numberOfCorrect = (int) (Math.random() * 100d) % numberOfAnswers;
//        numberOfCorrect += 1;

        for (int i = 0; i < numberOfCorrect; i++) {
            int rndCorrect = (int) (Math.random() * 100d) % correct.size();
            Answer answer = new Answer(correct.get(rndCorrect), fraction);
            answers.add(answer);
            correct.remove(rndCorrect);
        }

        for (int i = 0; i < numberOfAnswers - numberOfCorrect; i++) {
            int rndIncorrect = (int) (Math.random() * 100d) % incorrect.size();
            Answer answer = new Answer(incorrect.get(rndIncorrect), fractionNeg);
            answers.add(answer);
            incorrect.remove(rndIncorrect);
        }

        return answers;

    }
}
